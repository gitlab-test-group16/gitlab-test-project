# GitlLab Test Project

## Todo

1. Clone Repository
2. Create a branch with your name like `firstname-lastname`
3. Push your branch

4. Create a new feature branch off of your branch with the name in the form of `<firstname-lastname>-<feature>`
5. Change `index.html` so that the message matches your name.
6. Commit and push your changes to your feature branch.

7. Switch back to your branch with your name
8. Create a new freature branch (similar to step Nr. 4)
9. Change `styles.css` so that the text is written in the `Arial` font
10. Commit and push your changes to your feature branch.

11. Merge both your feature branches with your branch

## Solution for Command Line

1. `git clone git@gitlab.com:gitlab-test-group16/gitlab-test-project.git`
2. `git checkout -b remo-fischer`
3. `git push --set-upstream origin remo-fischer`

4. `git checkout -b remo-fischer-change-message-in-index`
5. In `index.html` change the name
6. `git commit -m "changed index.html to match my name"` and `git push --set-upstream origin remo-fischer-change-message-in-index`

7. `git checkout remo-fischer`
8. `git checkout -b remo-fischer-change-message-in-index`
9. In `styles.css` change `font-family` to `Arial`
10. `git commit -m "changed styles.css to show text in Arial"` and `git push --set-upstream origin remo-fischer-change-font`

11. `git checkout remo-fischer` + `git merge remo-fischer-change-message-in-index` + `git push` + `git merge remo-fischer-change-font` + `git push`
